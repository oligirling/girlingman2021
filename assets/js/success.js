function getCookie(name) {
    var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (match) return match[2];
}

const orderCookie = getCookie("theDetails");
const isNewOrderCookie = getCookie("isNewOrder");
const workerBaseUrl = 'https://girlingmanpayment.houseofapis.workers.dev' // http://127.0.0.1:4443
if (orderCookie == null) {
    setOrderDetails("No Order Found", '')
} else {
    setOrderDetails("Order Complete", orderCookie)
    if (isNewOrderCookie === 'true') {
        try {
            deleteNewOrderCookie()
            storeOrderDetails(orderCookie).then(r => console.log(r))

        } catch (e) {
            alert("Something has got wrong, screenshot this and send it to us: " + e.message ?? e)
        }

    }
}

async function storeOrderDetails(orderCookie)
{
    let response = await fetch(workerBaseUrl + '/create-order', {
        method: 'POST',
        body: "data="+JSON.stringify(orderCookie),
        headers: {
            "x-sausage": 1,
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/x-www-form-urlencoded"
        }
    })
    return await response.json()
}

function deleteNewOrderCookie()
{
    document.cookie = "isNewOrder=0; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}

function setOrderDetails(title, details = '')
{
    let div = document.getElementById('orderDetails')
    div.innerHTML += "<h1>" + title + "</h1>"
    if (details !== '') {
        details = JSON.parse(details)
        div.innerHTML += "<p>Total Paid: £" + details.total + "</p>"
        if (details.tent.price != 0) {
            div.innerHTML += "<p>Tent: " + details.tent.name + " (£" + details.tent.price + ")</p>"
        }
        let list = "<div class='success-tickets'>"
        details.tickets.forEach(ticket => {
            list += "<p>• 1x ticket for " + ticket.name + (ticket.beers == 0 ? '' : ' with 10 prepaid beers') +"</p>"
        })
        list += "</div>"
        div.innerHTML += list
    }


}

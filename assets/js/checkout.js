// This is your test publishable API key.
//const stripe = Stripe("pk_test_51KVEmPCilPixNrw38GJfZaXggtSBaVLAt6711eUz7zOXliszbbuVVrBjWS3e0v5fjfZ10aPOi0RI73XV8iwvroG400kNdhLaul"); // test
const stripe = Stripe("pk_live_51KVEmPCilPixNrw3tVUrFo5KOG7grYFfsDzgQz5lc3Z5krfqpZ09L4E243sn9uvWtKsTR8bpg57kyvx4DeJVbN7n00wjwwBEJ2"); // live


let elements;

const workerBaseUrl = 'https://girlingmanpayment.houseofapis.workers.dev' // http://127.0.0.1:4443
const workerVersion = 'prod'
const qty = document.getElementById("ticket-to-purchase-ticket-quantity");
const paymentTotal = document.getElementById('paymentTotal')

const dayTicket = document.getElementById("d-t");

function valid(id) {
    return document.getElementById(id).value !== "";
}

const validateEmail = (email) => {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
};


document.getElementById("ticket-to-purchase-submit").addEventListener('click', async function (event) {
    event.preventDefault()

    let dropdown = document.getElementById("ticket-to-purchase-ticket-quantity");
    let value = dropdown.options[dropdown.selectedIndex].dataset.human;
    let email
    for (let i = 0; i < parseInt(value); i++) {
        email = document.getElementById("human-email-" + i).value.trim()
        if (!valid("human-name-" + i)) {
            alert('Missing the name of human number ' + parseInt(i+1))
            return false;
        }
        if (!valid("human-email-" + i) || !validateEmail(email)) {
            alert('Missing valid email of human number ' + parseInt(i+1))
            return false;
        }
    }

    let total = document.getElementById('paymentTotal').value
    let response = await fetch(workerBaseUrl + '/payment?amount=' + total, {
        headers: {
            "x-sausage": 1,
            "x-version": workerVersion
        },
    })
    const { public_id } = await response.json();

    elements = stripe.elements({
        appearance: {
            theme: 'night',
            labels: 'floating'
        },
        clientSecret: public_id
    });
    const paymentElement = elements.create("payment");
    paymentElement.mount("#payment-element");
    setLoading(true);
    paymentElement.on('ready', function () {
        setLoading(false);
    });

    document.getElementById('ticket-pay-now-price').innerText = total;
    document.getElementById('start-payment').style.display = 'block';
    checkStatus();

})

document
    .querySelector("#payment-form")
    .addEventListener("submit", handleSubmit);


async function handleSubmit(e) {
    e.preventDefault();
    setLoading(true);
    let total = document.getElementById('paymentTotal').value
    let ticket = document.getElementById('ticket-to-purchase')

    try {
        const paymentIntent = await stripe.confirmPayment({
            elements,
            confirmParams: {
                return_url: window.location.origin + "/success.html",
            },
            redirect: 'if_required'
        });
        console.log(paymentIntent.paymentIntent)
        if (paymentIntent.hasOwnProperty('paymentIntent') && paymentIntent.paymentIntent.hasOwnProperty('status') && paymentIntent.paymentIntent.status === "succeeded") {
            console.log('payment finished')
            document.getElementById('congrats').style.display = 'block';
            document.getElementById('start-payment').style.display = 'none';
            const response = await storeOrderDetails(JSON.stringify(getOrderObj(total, ticket)))
            console.log(response)
        } else if (paymentIntent.hasOwnProperty('error')) {
            console.log('payment throw an error')
            throw new Error('There was an error: ' + paymentIntent.error.message)
        } else {
            throw new Error('payment didnt have status ' + paymentIntent)
        }
    } catch (err) {
        showMessage(err.message);
    }

    setLoading(false);
}

// Fetches the payment intent status after payment submission
async function checkStatus() {
    const clientSecret = new URLSearchParams(window.location.search).get(
        "payment_intent_client_secret"
    );

    if (!clientSecret) {
        return;
    }

    const { paymentIntent } = await stripe.retrievePaymentIntent(clientSecret);

    switch (paymentIntent.status) {
        case "succeeded":
            showMessage("Payment succeeded!");
            break;
        case "processing":
            showMessage("Your payment is processing.");
            break;
        case "requires_payment_method":
            showMessage("Your payment was not successful, please try again.");
            break;
        default:
            showMessage("Something went wrong.");
            break;
    }
}

// ------- UI helpers -------

function showMessage(messageText) {
    const messageContainer = document.querySelector("#payment-message");

    messageContainer.classList.remove("hidden");
    messageContainer.textContent = messageText;

    setTimeout(function () {
        messageContainer.classList.add("hidden");
        messageText.textContent = "";
    }, 10000);
}

// Show a spinner on payment submission
function setLoading(isLoading) {
    if (isLoading) {
        // Disable the button and show a spinner
        document.querySelector("#submit").disabled = true;
        document.querySelector("#spinner").classList.remove("hidden");
        document.querySelector("#button-text").classList.add("hidden");
    } else {
        document.querySelector("#submit").disabled = false;
        document.querySelector("#spinner").classList.add("hidden");
        document.querySelector("#button-text").classList.remove("hidden");
    }
}


async function storeOrderDetails(order) {
    let response = await fetch(workerBaseUrl + '/create-order', {
        method: 'POST',
        body: "data=" + JSON.stringify(order),
        headers: {
            "x-sausage": 1,
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "application/x-www-form-urlencoded"
        }
    })
    return await response.json()
}

function getOrderObj(total, ticket) {
    let humans = getTicketQty(ticket).tickets
    let tents = getTent(ticket)
    let orderDetails = []
    let dayTicket = document.getElementById('d-t').value
    let ticketPrice = dayTicket === "true"
        ? document.getElementById('d-t-p').value
        : document.getElementById('t-p').value

    for (let i = 0; i < humans; i++) {
        orderDetails[i] = {
            name: document.getElementById('human-name-' + i).value.trim(),
            email: document.getElementById('human-email-' + i).value.trim(),
            price: ticketPrice,
            isDayTicket: dayTicket,
            beers: document.getElementById('human-beers-' + i).value,
        }
    }
    return {
        payee: {
            name: orderDetails[0].name,
            email: orderDetails[0].email
        },
        referredBy: document.getElementById('referrer').value,
        total: total,
        tent: tents,
        tickets: orderDetails
    }
}

const allTents = {
    "4m-db": '<option value="125" data-tent="4 meter (double bed)" data-code="4m-db">4 meter (double bed): £125</option>',
    "5m-2db": '<option value="180" data-tent="5 meter (2x double beds)" data-code="5m-2db">5 meter (2x double beds): £180</option>',
    "5m-2db-wfs": '<option value="200" data-tent="5 meter (2x double beds with wood fire stove)" data-code="5m-2db-wfs">5 meter (2x double beds + wood fire stove): £200</option>',
    "6m-no-furn": '<option value="220" data-tent="6 meter (no furniture)" data-code="6m-no-furn">6 meter (no furniture): £220</option>',
    "6m-2db-2sb-wfs": '<option value="280" data-tent="6 meter (2x double beds with 2x single beds + wood fire stove)" data-code="6m-2db-2sb-wfs">6 meter (2x double beds + 2x single beds + wood fire stove): £280</option>',
}
async function populateTents() {
    let response = await fetch(workerBaseUrl + '/list-accom', {
        method: 'GET',
        headers: {
            "x-sausage": 1
        }
    })
    const availibleTents = await response.json()
    for (const [key, value] of Object.entries(availibleTents.accom)) {
        document.getElementById('tents').innerHTML += getTentHtml((value > 0))[key]
    }
}

function getTentHtml(avail) {

    return {
        "4m-db": '<option value="125" data-tent="4 meter (double bed)" data-code="4m-db" ' + (avail ? '' : "disabled") + '>4 meter (double bed): £125' + (avail ? '' : " - SOLD OUT") + '</option>',
        "5m-2db": '<option value="180" data-tent="5 meter (2x double beds)" data-code="5m-2db" ' + (avail ? '' : "disabled") + '>5 meter (2x double beds): £180' + (avail ? '' : " - SOLD OUT") + '</option>',
        "5m-2db-wfs": '<option value="200" data-tent="5 meter (2x double beds with wood fire stove)" data-code="5m-2db-wfs" ' + (avail ? '' : "disabled") + '>5 meter (2x double beds + wood fire stove): £200' + (avail ? '' : " - SOLD OUT") + '</option>',
        "6m-no-furn": '<option value="220" data-tent="6 meter (no furniture)" data-code="6m-no-furn" ' + (avail ? '' : "disabled") + '>6 meter (no furniture): £220' + (avail ? '' : " - SOLD OUT") + '</option>',
        "6m-2db-2sb-wfs": '<option value="280" data-tent="6 meter (2x double beds with 2x single beds + wood fire stove)" data-code="6m-2db-2sb-wfs" ' + (avail ? '' : "disabled") + '>6 meter (2x double beds + 2x single beds + wood fire stove): £280' + (avail ? '' : " - SOLD OUT") + '</option>',
    }
}
populateTents()
const urlParams = new URLSearchParams(window.location.search)
if (urlParams.has('ref') && urlParams.get('ref') !== '') {
    document.getElementById('referrer').value = urlParams.get('ref')
}
qty.addEventListener('change', event => {
    let ticket = document.getElementById('ticket-to-purchase')
    //ticket.style.backgroundColor = "#0e0035";
    let nameArea = ticket.getElementsByClassName('humans-list')[0]
    nameArea.innerHTML = ''
    let amountOfHumans = event.target.options[event.target.selectedIndex].dataset.human
    event.target[0].disabled = true
    for (let i = 0; i < amountOfHumans; i++) {
        nameArea.innerHTML += getHumanDetailsHtml(i)
    }
    if (event.target.options[event.target.selectedIndex].dataset.day === "true") {
        dayTicket.value = "true"
    } else {
        dayTicket.value = "false"
    }
    handleChange(ticket)
    const beers = document.querySelectorAll(".human-beers-select");
    beers.forEach(ele => ele.addEventListener('change', event => handleChange(ticket)));
});
function handleChange(ticket) {
    let paymentButton = ticket.getElementsByClassName('payment')[0]
    let total = getTotal(ticket)
    paymentButton.disabled = false
    paymentButton.innerText = 'Pay £' + total
    paymentTotal.value = total
}
const tentsForm = document.querySelectorAll(".ticket-tent");
tentsForm.forEach(el => el.addEventListener('change', event => {
    let ticket = document.getElementById('ticket-to-purchase')
    //ticket.style.backgroundColor = "#0e0035";
    handleChange(ticket)
}));
function getTotal(ticket) {
    let ticketBeers = ticket.querySelectorAll(".human-beers-select");
    let totalExtras = 0
    ticketBeers.forEach(select => {
        totalExtras += parseFloat(select.value)
    })
    let ticketValue = getTicketQty(ticket).price
    let tentValue = getTent(ticket).price
    totalExtras += parseFloat(ticketValue) + parseFloat(tentValue)
    return totalExtras.toFixed(2)
}
function getTicketQty(ticket) {
    let price = ticket.getElementsByClassName('ticket-quantity')[0]
    return {
        price: price.value,
        tickets: price.options[price.selectedIndex].dataset.human
    }
}
function getTent(ticket) {
    let price = ticket.getElementsByClassName('ticket-tent')[0]
    return {
        price: price.value,
        name: price.options[price.selectedIndex].dataset.tent,
        code: price.options[price.selectedIndex].dataset.code
    }
}
function getHumanDetailsHtml(index) {
    const namePh = index === 0 ? 'Enter payees name' : 'Enter human' + (index + 1) + 's name';
    const emailPh = index === 0 ? 'Enter payees email' : 'Enter human' + (index + 1) + 's email';
    return "<div class='human-entry'>" +
        "<div class='human-entry-name col-md-3 col-sm-9'>" +
        "<input type='text' required id='human-name-" + index + "' name='human-name' placeholder='" + namePh + "'>" +
        "</div>" +
        "<div class='human-entry-name col-md-3 col-sm-9'>" +
        "<input type='text' required id='human-email-" + index + "' name='human-email' placeholder='" + emailPh + "'>" +
        "</div>" +
        "<div class='human-entry-beers col-md-2 col-sm-2'>" +
        "<select class='human-beers-select' id='human-beers-" + index + "'>" +
        "<option value='0' selected>No prepaid beers</option>" +
        "<option value='22.50'>10 beers (+£22.50)</option>" +
        "</select>" +
        "</div>" +
        "</div>"
}

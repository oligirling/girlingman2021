let searchParams = new URLSearchParams(window.location.search)
if (searchParams.has('option') && searchParams.has('amount') && searchParams.has('humans')) {
  const options = document.getElementsByClassName('option');
  const amounts = document.getElementsByClassName('amount');
  const humans = document.getElementsByClassName('humans');
  var i;
  for (i = 0; i < amounts.length; i++) {
    amounts[i].innerText = searchParams.get('amount');
  }
  var k;
  for (k = 0; k < options.length; k++) {
    options[k].innerText = searchParams.get('option');
  }
  var j;
  for (j = 0; j < humans.length; j++) {
    humans[j].innerText = searchParams.get('humans');
  }
  if (searchParams.get('option').indexOf("4HOR") >= 0 || searchParams.get('option').indexOf("5HOR") >= 0) {
    const tent = document.getElementsByClassName('tent');
    var p;
    for (p = 0; p < tent.length; p++) {
      tent[p].innerText = '1x Home in Halls Of Residence'
    }
    if (searchParams.get('option').indexOf("DoubleDown") >= 0) {
      document.getElementById('extras').innerText = 'Extras: Camp beds, Bunting and Fairy lights (bring your own bedding)'
    }
    if (searchParams.get('option').indexOf("DoubleDaddy") >= 0) {
      document.getElementById('extras').innerText = 'Extras: Rug, Table, Camp beds, Bunting and Fairy lights (bring your own bedding)'
    }
  }
  if (searchParams.get('option') === 'Party Tuition') {
    document.getElementById('night-ticket').innerText = ' (Single Night)'
  }
} else {
  console.log('No option')
}

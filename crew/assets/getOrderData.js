const workerBaseUrl = 'https://girlingmanpayment.houseofapis.workers.dev' // http://127.0.0.1:4443
const allOrdersName = "allOrders"

async function getOrderData() {
    if (window.localStorage.getItem(allOrdersName) === null) {
        console.log('storage null so getting orders')
        const response = await fetch(workerBaseUrl + '/list-orders', {
            headers: {
                "x-sausage": 1,
            }
        })
        const data = await response.json()
        const orders = data.orders
        const jsonString = JSON.stringify(orders)
        console.log('jsonString', jsonString)
        var now = new Date();
        now.setTime(now.getTime() + 3600 * 1000);
        window.localStorage.setItem(allOrdersName, jsonString)
        return orders

    } else {
        console.log('storage not null so getting orders from cookie')
        let orders = window.localStorage.getItem(allOrdersName)
        return JSON.parse(orders)
    }
}

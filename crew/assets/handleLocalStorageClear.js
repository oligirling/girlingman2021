var hours = 1;
var now = new Date().getTime();
var setupTime = window.localStorage.getItem('setupTime');
if (setupTime == null) {
    window.localStorage.setItem('setupTime', now)
} else {
    if (now-setupTime > hours*60*60*1000) {
        window.localStorage.clear()
        window.localStorage.setItem('setupTime', now);
    }
}

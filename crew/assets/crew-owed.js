let tableRef = document.getElementById('order-table').getElementsByTagName('tbody')[0]
let totalTop = document.getElementById('total')
let totalPeople = document.getElementById('total-people')
let totalFromBeerSales = document.getElementById('total-beer')
let amountFromTents = document.getElementById('tent-total')
let amountOfPeople = document.getElementById('people-total')

getOrderData()
    .then(orderData => {
            handleData(orderData)
        }
    )

function handleData(orders) {
    let index = 1
    let total = 0
    let totalFromTents = 0
    let totalFromJustTickets = 0
    let totalFromJustBeers = 0
    for (const [key, value] of Object.entries(orders)) {
        console.log(key, value)
        let orderId = key
        let dataObj = JSON.parse(value)
        let payeeEmail = dataObj.To
        let dataObj2 = dataObj.TemplateModel
        let payeeName = dataObj2.name
        let referredBy = dataObj2.referred_by === "" ? "" : dataObj2.referred_by
        let date = (new Date(parseInt(orderId))).toLocaleString()
        let paidItems = dataObj2.receipt_details
        let totalPaid = dataObj2.total
        let peopleInTent = dataObj2.peopleInTent
        let tent = dataObj2.tentData
        let hoomans = dataObj2.listOfHumans

        hoomans.forEach(hoomanName => {

            let newRow = tableRef.insertRow(tableRef.rows.length)
            newRow.innerHTML = "<tr>\n" +
                "    <th scope=\"row\">" +  index + "</th>\n" +
                "    <td>" +  hoomanName.name + "<br><small>" + hoomanName.email + "</small></td>\n" +
                "    <td><small>🍺 - x | 🍷 - x | 🥃 - x<br>🌭 - x | 🍔 - x | 🐔 - x<br>🍟 - x | 🥑 - x</small></td>\n" +
                "    <td>£" + hoomanName.beers  + "</td>\n" +
                "    <td>£x (referred 3 people)<br>£x (helped out)</td>\n" +
                "    <td>£x - UNPAID</td>\n" +
                "    <td><button type=\"button\" class=\"btn btn-primary\" data-bs-toggle=\"modal\" data-bs-target=\"#exampleModal\" " +
                "           data-bs-humanname=\"" + hoomanName.name +"\" data-bs-humanemail=\"" + hoomanName.email +"\"" +
                "           data-bs-humanbeer=\"" + (hoomanName.beers > 0 ? 10 : 0) +"\">Edit</button><button type=\"button\" class=\"btn btn-warning\">Send Invoice</button></td>\n" +
                "  </tr>"

            let isDayTicket = hoomanName.hasOwnProperty('isDayTicket') && hoomanName.isDayTicket === 'true'

            totalFromJustTickets += parseFloat(hoomanName.price)
            totalFromJustBeers += parseFloat(hoomanName.beers)
            index++
        })

        total += parseFloat(dataObj2.total)
        totalFromTents += parseFloat(tent.price)

    }

    /*totalTop.innerText = total
    totalPeople.innerText = totalFromJustTickets
    totalFromBeerSales.innerText = totalFromJustBeers
    amountFromTents.innerText = totalFromTents
    amountOfPeople.innerText = tableRef.rows.length*/
}

var exampleModal = document.getElementById('exampleModal')
exampleModal.addEventListener('show.bs.modal', function (event) {
    // Button that triggered the modal
    let button = event.relatedTarget
    // Extract info from data-bs-* attributes
    let name = button.getAttribute('data-bs-humanname')
    let email = button.getAttribute('data-bs-humanemail')
    let beer = button.getAttribute('data-bs-humanbeer')
    // If necessary, you could initiate an AJAX request here
    // and then do the updating in a callback.
    //
    // Update the modal's content.
    let modalTitle = exampleModal.querySelector('.modal-title')
    let modalButton = exampleModal.querySelector('#model-update-button')
    let nameEmail = exampleModal.querySelector('#model-human-name-email')
    let prepaidBeer = exampleModal.querySelector('#model-human-prepaid-beer')

    modalTitle.textContent = 'Edit Money Owed for ' + name
    modalButton.textContent = 'Save for ' + name
    nameEmail.value = name + " (" + email + ")"
    prepaidBeer.value = beer
})

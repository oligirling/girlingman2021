let tableRef = document.getElementById('order-table').getElementsByTagName('tbody')[0]
let totalTop = document.getElementById('total')
let totalPeople = document.getElementById('total-people')
let totalFromBeerSales = document.getElementById('total-beer')
let amountFromTents = document.getElementById('tent-total')
let amountOfPeople = document.getElementById('people-total')

getOrderData()
    .then(orderData => {
        handleData(orderData)
    }
)


function handleData(orders) {
    let index = 1
    let total = 0
    let totalFromTents = 0
    let totalFromJustTickets = 0
    let totalFromJustBeers = 0
    for (const [key, value] of Object.entries(orders)) {
        console.log(key, value)
        let orderId = key
        let dataObj = JSON.parse(value)
        let payeeEmail = dataObj.To
        let dataObj2 = dataObj.TemplateModel
        let payeeName = dataObj2.name
        let referredBy = dataObj2.referred_by === "" ? "" : dataObj2.referred_by
        let date = (new Date(parseInt(orderId))).toLocaleString()
        let paidItems = dataObj2.receipt_details
        let totalPaid = dataObj2.total
        let peopleInTent = dataObj2.peopleInTent
        let tent = dataObj2.tentData
        let hoomans = dataObj2.listOfHumans

        hoomans.forEach(hoomanName => {

            let newRow = tableRef.insertRow(tableRef.rows.length)
            let isDayTicket = hoomanName.hasOwnProperty('isDayTicket') && hoomanName.isDayTicket === 'true'

            newRow.innerHTML = "<tr>\n" +
                "    <th scope=\"row\">" +  index + "</th>\n" +
                "    <td>" +  orderId + "<br><small>Purchased by: " + payeeName + "</small></td>\n" +
                "    <td>" +  date + "</td>\n" +
                "    <td>" +  hoomanName.name + "<br><small>" + hoomanName.email + "</small></td>\n" +
                (tent.code ? "    <td>1x " +  tent.code + "<br><small>With: " + peopleInTent.join(", ") + "</small></td>\n" : '<td>Nope</td>') +
                "    <td>£" + hoomanName.beers  + "</td>\n" +
                "    <td>£" +  hoomanName.price + (isDayTicket ? "<br><small>Day Ticket</small>" : "") + "</td>\n" +
                "    <td>" + referredBy  + "</td>\n" +
                "  </tr>"

            totalFromJustTickets += parseFloat(hoomanName.price)
            totalFromJustBeers += parseFloat(hoomanName.beers)
            index++
        })

        total += parseFloat(dataObj2.total)
        totalFromTents += parseFloat(tent.price)

    }

    totalTop.innerText = total
    totalPeople.innerText = totalFromJustTickets
    totalFromBeerSales.innerText = totalFromJustBeers
    amountFromTents.innerText = totalFromTents
    amountOfPeople.innerText = tableRef.rows.length
}
